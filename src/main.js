import * as THREE from 'three';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import { OBJLoader } from 'three/addons/loaders/OBJLoader.js';
import { MTLLoader } from 'three/addons/loaders/MTLLoader.js';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';

const scene = new THREE.Scene();

// Create a camera
const camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    1,
    5000
);

camera.position.z = 1200;

// Create a renderer
const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setClearColor( 0xffffff, 0 )
document.body.appendChild(renderer.domElement);

const controls = new OrbitControls( camera, renderer.domElement );

const mtlLoader = new MTLLoader();
mtlLoader.setMaterialOptions( {
    // This property looks relevant if exprted from meshlab
    // https://threejs.org/docs/#examples/en/loaders/MTLLoader.setMaterialOptions
    invertTrProperty: true
})
mtlLoader.load("/materials.mtl", function(materials) {
    materials.preload();


    const objLoader = new OBJLoader();
    objLoader.setMaterials(materials);

    // Load the CAD file
    objLoader.load(
        "/IBUCS.obj",
        function (object) {
            // Add the imported object to the scene
            // object.scene.traverse(item => {
            //     item.frustumCulled = false
            // })

            // object.scene.background = new THREE.Color(0xff0000)
            scene.add(object);

            renderer.render(scene, camera);
        },
        function (xhr) {
            // Progress callback (optional)
            console.log((xhr.loaded / xhr.total) * 100 + "% loaded");
        },
        function (error) {
            // Error callback (optional)
            console.error("Error loading CAD file:", error);
        }
    );
});



// Create an animation loop
function animate() {
    requestAnimationFrame(animate);

    // Rotate the object (optional)
    // if (scene.children.length > 0) {
    //     scene.children[0].rotation.y += 0.01;
    // }

    controls.update();

    // Render the scene with the camera
    renderer.render(scene, camera);
}

// Start the animation loop
animate();