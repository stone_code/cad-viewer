## Getting Started

1. If you haven't already, install NodeJS from here https://nodejs.org/en

2. Update npm (javascript package manager) by running this command from your terminal

```
npm install --global npm
```

3. Run a local web server - again from the project root directory, run this command.

```
npx serve ./src
```